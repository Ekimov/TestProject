package com.example.igor.myapplication;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabClickListener;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity
       {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //1
        int palindromNumber = findPalindrome();

        //2
        int sumDigist = getSumDigist(100);

        //3
        int[][] a = {
                {1, 4},
                {2, 5},
                {7, 3},
                {4, 6},
                {7, 7}
        };
        findSubArray(a);
    }

           private int getSumDigist(int count) {
               int sumDigest =0;
               String digitsString =factorial(count).toString();
               char [] charArray = digitsString.toCharArray();
               for(int i=0;i<charArray.length; i++){
                   sumDigest+=Integer.parseInt(charArray[i]+"");
               }
               return sumDigest;
           }

           public BigInteger factorial(int n)
           {
               if (n == 0) return BigInteger.ONE;
               return BigInteger.valueOf(n).multiply(factorial(n-1));
           }


    private void findSubArray(int[][] a) {
        int subArray[][] = new int[2][2];
        int minMassIndex=-10;
        for(int i = 0; i<a.length; i++){
            if(a[i][0]<a[i][1]){
               minMassIndex=i;
            };

            if(a[i][0]>a[i][1]){
                if(minMassIndex==(i-1)){
                    subArray[0][0]=a[minMassIndex][0];
                    subArray[0][1]=a[minMassIndex][1];
                    subArray[1][0]=a[i][0];
                    subArray[1][1]=a[i][1];
                }
            }
        }
    }


    private int findPalindrome() {
        ArrayList<Integer> palindroms = new ArrayList<>();
        for(int i = 999;i>100; i--){
            for(int k = 999;k>100; k--){
                int res = i*k;
                if(isPalindrom(Integer.toString(res))){
                    palindroms.add(res);
                }
            }
        }
        Collections.sort(palindroms, new Comparator<Integer>() {
            @Override
            public int compare(Integer int1, Integer int2) {
                return (int1>int2 ? -1 : (int1==int2 ? 0 : 1));
            }
        });
         return palindroms.get(0);
    }

    private boolean isPalindrom(String str) {
        return str.equals(new StringBuilder().append(str).reverse().toString());
    }

}
